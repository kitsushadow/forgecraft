=========================
       FORGECRAFT
=========================
The development of this mod is for the sole purpose of providing an avenue to learn and apply new knowledge in java. 
I have been working on this mod publicly since December, which was the time of its first release. I have, however, been 
working on this mod privately for about 3 years going into 4. Feel free to contact me on my forum thread with questions,
comments, concerns etc. I appreciate all feedback!

===FORUM THREAD===
http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/wip-mods/2296334-wip-kitsus-forgecraft-blacksmith-style-forging

Thanks and Enjoy!

===Inspiration Documentation===
https://docs.google.com/document/d/1bF8stKzhlouV_xZXlVYgxQPfbwhWJTtaf7z_QXYYEXk/edit?usp=sharing

https://docs.google.com/document/d/10ZNbGOZ-ytfadjEnhMgnBPEErvvbGUkW_SMU6kX9yQA/edit?usp=sharing

Forge source installation
=========================
To install this source code for development purposes, extract this zip file.
It ships with a demonstration mod. Run 'gradlew setupDevWorkspace' to create
a gradle environment primed with FML. Run 'gradlew eclipse' or 'gradlew idea' to
create an IDE workspace of your choice.
Refer to ForgeGradle for more information about the gradle environment
Note: On macs or linux you run the './gradlew.sh' instead of 'gradlew'